<?php

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_home", "", TRUE);
        $this->load->model("m_port", "", TRUE);
    }

    public function index()
    {
        $data['Port'] = $this->m_port->getAllport();
        $this->load->view('home', $data);
    }

    public function tambah()
    {
        $data = array(
            'nama' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'subject' => $this->input->post('subject'),
            'pesan' => $this->input->post('message'),
            'tanggal' => $this->input->post('tanggal')
        );
        $this->m_home->tambah($data, 'user');
        redirect('home');
    }

    public function visitor() 
    {
        $ip = $this->input->ip_address(); // Mendapatkan IP user
        $date = date("Y-m-d"); // Mendapatkan tanggal sekarang
        $waktu = time(); //
        $timeinsert = date("Y-m-d H:i:s");

    }
}

    