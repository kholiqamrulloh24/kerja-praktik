<?php

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_admin", "", TRUE);
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $data['Admin'] = $this->m_admin->getAlladmin();
            $this->load->view("register", $data);
        } else {
            $this->load->view('login');
        }
    }

    public function tambah()
    {
        $data = array(
            'number' => $this->input->post('number'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        $this->m_admin->tambah($data, 'admin');
        redirect('register');
    }

    public function edit()
    {
        $id = $this->input->post('id');
        $this->m_admin->edit($id);
        redirect('register');
    }

    public function hapus($id)
    {
        $this->m_admin->hapus($id);
        redirect('register');
    }
}
