<?php

class TambahPort extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("m_port", "", TRUE);
    }

    public function index()
    {
        $this->load->view('tambahPortofolio');
    }

    public function tambah()
    {
        $img1 = $_FILES['image1'];
        if ($img1=''){}else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image1');
            $img1 = $this->upload->data('file_name');
        }

        $img2 = $_FILES['image2'];
        if ($img2=''){}else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image2');
            $img2 = $this->upload->data('file_name');
        }

        $img3 = $_FILES['image3'];
        if ($img3=''){}else {
            $config['upload_path'] = './assets/img/upload';
            $config['allowed_types'] = 'jpg|png|gif';

            $this->load->library('upload', $config);
            $this->upload->do_upload('image3');
            $img3 = $this->upload->data('file_name');
        }

        $data = array(
            'judul' => $this->input->post('judul'),
            'img1' => $img1,
            'img2' => $img2,
            'img3' => $img3,
            'capt1' => $this->input->post('caption1'),
            'capt2' => $this->input->post('caption2'),
            'capt3' => $this->input->post('caption3'),
            'capt4' => $this->input->post('caption4'),
            'capt5' => $this->input->post('caption5')          
        );

        $this->m_port->tambah($data, 'portofolio');
        redirect('portofolio');
    }
}