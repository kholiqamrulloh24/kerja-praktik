<?php

class m_port extends CI_Model
{
    public function getAllport()
    {
        return $this->db->get('portofolio')->result_array();
    }

    public function tambah($data)
    {
        $this->db->insert('portofolio', $data);
    }

    public function edit($id)
    {
        $data = [
            "judul" => $this->input->post('judul'),
            "image" => $this->input->post('image'),
            "caption" => $this->input->post('caption')
        ];
        $this->db->where('number', $id);
        $this->db->update('portofolio', $data);
    }

    function hapus($id)
    {
        $this->db->delete('portofolio', ['number' => $id]);
    }
}